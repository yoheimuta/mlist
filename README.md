# README #

memberlist を使った、ノード間のクラスター管理を確認する

### 依存ライブラリをインストールする

```sh
[great@Mac-158() mlist]$ gpm install
>> Getting package github.com/hashicorp/memberlist
>> Getting package github.com/codegangsta/martini
>> Setting github.com/hashicorp/memberlist to version
>> Setting github.com/codegangsta/martini to version
>> All Done
```

### vagrant host を立ち上げる

```sh
[great@Mac-158() mlist]$ vagrant up
[great@Mac-158() mlist]$ vagrant ssh n1
```

### vagrant host に go install する

- go install は 10 分くらいかかるので、Vagrantfile でプロビジョニングしないで 1 ホストでだけ実施
 - binary が出来たら他のホストはそれを使いまわす

```sh
vagrant@precise64:~$ wget https://raw.github.com/kwmt/goinstall/master/goinstall.sh
--2014-12-04 15:00:09--  https://raw.github.com/kwmt/goinstall/master/goinstall.sh
Resolving raw.github.com (raw.github.com)... 103.245.222.133
Connecting to raw.github.com (raw.github.com)|103.245.222.133|:443... connected.
HTTP request sent, awaiting response... 301 Moved Permanently
Location: https://raw.githubusercontent.com/kwmt/goinstall/master/goinstall.sh [following]
--2014-12-04 15:00:10--  https://raw.githubusercontent.com/kwmt/goinstall/master/goinstall.sh
Resolving raw.githubusercontent.com (raw.githubusercontent.com)... 103.245.222.133
Connecting to raw.githubusercontent.com (raw.githubusercontent.com)|103.245.222.133|:443... connected.
HTTP request sent, awaiting response... 200 OK
Length: 2180 (2.1K) [text/plain]
Saving to: `goinstall.sh'

100%[========================================================================================================================================>] 2,180       --.-K/s   in 0s

2014-12-04 15:00:10 (21.5 MB/s) - `goinstall.sh' saved [2180/2180]

vagrant@precise64:~$ wget https://raw.github.com/kwmt/goinstall/master/gosetting.sh
--2014-12-04 15:00:14--  https://raw.github.com/kwmt/goinstall/master/gosetting.sh
Resolving raw.github.com (raw.github.com)... 103.245.222.133
Connecting to raw.github.com (raw.github.com)|103.245.222.133|:443... connected.
HTTP request sent, awaiting response... 301 Moved Permanently
Location: https://raw.githubusercontent.com/kwmt/goinstall/master/gosetting.sh [following]
--2014-12-04 15:00:15--  https://raw.githubusercontent.com/kwmt/goinstall/master/gosetting.sh
Resolving raw.githubusercontent.com (raw.githubusercontent.com)... 103.245.222.133
Connecting to raw.githubusercontent.com (raw.githubusercontent.com)|103.245.222.133|:443... connected.
HTTP request sent, awaiting response... 200 OK
Length: 1248 (1.2K) [text/plain]
Saving to: `gosetting.sh'

100%[========================================================================================================================================>] 1,248       --.-K/s   in 0s

2014-12-04 15:00:15 (39.1 MB/s) - `gosetting.sh' saved [1248/1248]

vagrant@precise64:~$ chmod +x goinstall.sh gosetting.sh
vagrant@precise64:~$ sudo ./goinstall.sh
usage: sudo ./goinstall.sh login_user
vagrant@precise64:~$ sudo ./goinstall.sh vagrant
go is not Installed. Continue...
...
vagrant@precise64:~$ /usr/local/go/bin/go version
go version go1.3.3 linux/amd64
vagrant@precise64:~$ source ~/.bashrc
vagrant@precise64:~$ go version
The program 'go' is currently not installed.  You can install it by typing:
sudo apt-get install golang-go
vagrant@precise64:~$ echo "PATH=$PATH:/usr/local/go/bin" >> ~/.bashrc
vagrant@precise64:~$ source ~/.bashrc
vagrant@precise64:~$ go version
go version go1.3.3 linux/amd64
```

### vagrant host で mlist のバイナリを作成する

```
vagrant@precise64:/vagrant$ ls
Godeps  src  Vagrantfile
vagrant@precise64:/vagrant$ export GOPATH="/vagrant/.godeps:/vagrant/"
vagrant@precise64:/vagrant$ go install mlist
```

- `vagrant reload` したら `go install mlist` が失敗するようになった場合

```
vagrant@precise64:~$ export GOPATH="/vagrant/.godeps:/vagrant/"
vagrant@precise64:~$ go install mlist
go install mlist: open /usr/local/go/bin/mlist: permission denied
vagrant@precise64:~$ unset GOBIN //  If the GOBIN environment variable is set, commands are installed to the directory it names instead of DIR/bin.
vagrant@precise64:~$ go install mlist // これで OK
```

### vagrant host で mlist バイナリを起動する

- [hostname はピア間でユニークである必要](https://github.com/hashicorp/memberlist/issues/6) とのこと

```sh
// hostname を設定
vagrant@precise64:/vagrant$ sudo hostname 172.20.20.10
vagrant@precise64:/vagrant$ hostname
172.20.20.10
// 起動
vagrant@precise64:/vagrant$ MEMBERLIST_ADDR=172.20.20.10 bin/mlist
2014/12/04 15:33:32 Add peer: 172.20.20.10
2014/12/04 15:33:32 Current peers: [172.20.20.10]
[martini] listening on :3000 (development)
// n2 が join したことを検知
2014/12/04 15:33:35 [DEBUG] memberlist: Responding to push/pull sync with: 172.20.20.11:43898
2014/12/04 15:33:35 Add peer: 172.20.20.11
2014/12/04 15:33:35 Current peers: [172.20.20.10 172.20.20.11]
// n2 が down したことを検知して `suspect` にする
2014/12/04 15:33:43 [INFO] memberlist: Suspect 172.20.20.11 has failed, no acks received
2014/12/04 15:33:45 [INFO] memberlist: Suspect 172.20.20.11 has failed, no acks received
2014/12/04 15:33:46 [INFO] memberlist: Suspect 172.20.20.11 has failed, no acks received
// n2 が down したことを検知して `dead` にする
2014/12/04 15:33:48 [INFO] memberlist: Marking 172.20.20.11 as failed, suspect timeout reached
2014/12/04 15:33:48 Remove peer: 172.20.20.11
2014/12/04 15:33:48 Current peers: [172.20.20.10]
2014/12/04 15:33:48 [INFO] memberlist: Suspect 172.20.20.11 has failed, no acks received
// n2 が join したことを検知
2014/12/04 15:36:20 [DEBUG] memberlist: Responding to push/pull sync with: 172.20.20.11:43899
2014/12/04 15:36:20 Add peer: 172.20.20.11
2014/12/04 15:36:20 Current peers: [172.20.20.10 172.20.20.11]
2014/12/04 15:36:49 [DEBUG] memberlist: Initiating push/pull sync with: 172.20.20.11:7946
2014/12/04 15:37:10 [DEBUG] memberlist: Responding to push/pull sync with: 172.20.20.11:43900
2014/12/04 15:37:19 [DEBUG] memberlist: Initiating push/pull sync with: 172.20.20.11:7946
```

```sh
// hostname を設定
vagrant@precise64:/vagrant$ sudo hostname 172.20.20.11
// n1 クラスタに join
vagrant@precise64:/vagrant$ MEMBERLIST_ADDR=172.20.20.11 JOIN_TO=172.20.20.10 bin/mlist
2014/12/04 15:33:35 Add peer: 172.20.20.11
2014/12/04 15:33:35 Current peers: [172.20.20.11]
2014/12/04 15:33:35 [DEBUG] memberlist: Initiating push/pull sync with: 172.20.20.10:7946
2014/12/04 15:33:35 Add peer: 172.20.20.10
2014/12/04 15:33:35 Current peers: [172.20.20.11 172.20.20.10]
[martini] listening on :3000 (development)
// shutdown
...
// n1 クラスタに join
vagrant@precise64:/vagrant$ MEMBERLIST_ADDR=172.20.20.11 JOIN_TO=172.20.20.10 bin/mlist
2014/12/04 15:36:20 Add peer: 172.20.20.11
2014/12/04 15:36:20 Current peers: [172.20.20.11]
2014/12/04 15:36:20 [DEBUG] memberlist: Initiating push/pull sync with: 172.20.20.10:7946
2014/12/04 15:36:20 Add peer: 172.20.20.10
2014/12/04 15:36:20 Current peers: [172.20.20.11 172.20.20.10]
[martini] listening on :3000 (development)
2014/12/04 15:36:49 [DEBUG] memberlist: Responding to push/pull sync with: 172.20.20.10:56843
2014/12/04 15:37:10 [DEBUG] memberlist: Initiating push/pull sync with: 172.20.20.10:7946
2014/12/04 15:37:19 [DEBUG] memberlist: Responding to push/pull sync with: 172.20.20.10:56844
2014/12/04 15:37:40 [DEBUG] memberlist: Initiating push/pull sync with: 172.20.20.10:7946
2014/12/04 15:37:49 [DEBUG] memberlist: Responding to push/pull sync with: 172.20.20.10:56845
2014/12/04 15:38:10 [DEBUG] memberlist: Initiating push/pull sync with: 172.20.20.10:7946
2014/12/04 15:38:19 [DEBUG] memberlist: Responding to push/pull sync with: 172.20.20.10:56846
2014/12/04 15:38:40 [DEBUG] memberlist: Initiating push/pull sync with: 172.20.20.10:7946
2014/12/04 15:38:49 [DEBUG] memberlist: Responding to push/pull sync with: 172.20.20.10:56847
```

# memberlist とは

### 機能

- cluster membership
    - サーバーの追加と削除を検知して通知をする
- node failure detection
    - サーバーが応答しなくなった場合に正しく検知することが出来る
    - ネットワークが障害になっている場合にサーバーのダウンを判断できる

### プロトコル

- gossip based protocol を使う
- SWIM 論文に依拠している

### 設定

- 情報の伝播と収束の速度を上げると帯域を食いつぶすのでバランスが大事

### 動作

- はじめに`node`とともにそれが所属する`cluster`が生成され、その後そのクラスターに新たなノードが追加されてくる
    - 新しいノードはすでに所属しているノードのアドレスを指定することで、そのクラスターに`join`出来る
    - それぞれのノードの生存確認を`gossiping`するために、新しいノードは既存ノードと TCP 接続で `full state sync` する
- `Gossip` は `TCP` と `UDP` 接続により行われる
    - 基本的には `UDP`接続により実現され、設定により一定の `fanout` と `interval` で行われる
    - それによってノードが増えてもネットワーク利用量が急激に増えたりはしない
    - 加えて、周期的に `TCP` 接続による完全な状態交換 `Complete state exchange` をランダムなノードと行う
- `Failure Detection` は設定された間隔で周期的なランダム `probing` によって実現される
    - この際に RTT の数倍程度の時間応答が返ってこなければ、`indirect probe` を行う
    - これは複数のランダムなノードに対して探索を依頼して実際に行うこと
    - これも失敗すると、対象のノードは `suspicious` マークがつけられて、この情報はクラスターに対して伝達される
    - この時点では suspicious node はまだノードのメンバーだが、一定の時間内にこれに反論がなければ、`dead` 扱いになってそれも伝達される

### 関連するプロダクト

- [serf](https://www.serfdom.io/) は membership を使って実装されている
    - membership を使ったノード間のクラスタ管理機能（= ゴシッププロトコルによる伝搬）を外部コマンドと連携して使えるようにしたものという印象
    - [この記事によれば](http://blog.livedoor.jp/sonots/archives/35397486.html)cluster への `join` ' leave` `failed` イベント通知時に任意のスクリプトを実行することが出来る
    - ex) /etc/hosts を書き換えるスクリプトを登録すれば、サーバー追加時にこのバイナリを起動すれば全ホスト間で最新の /etc/hosts を更新・共有することが出来るようになる
