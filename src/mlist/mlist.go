package main

import (
	"log"
	"os"
	"strings"

	"github.com/codegangsta/martini"
	"github.com/hashicorp/memberlist"
)

type eventDelegate struct {
	peers []string
}

func (e *eventDelegate) NotifyJoin(node *memberlist.Node) {
	uri := node.Addr.String()
	e.removePeer(uri)
	e.peers = append(e.peers, uri)
	log.Print("Add peer: " + uri)
	log.Printf("Current peers: %v", e.peers)
}

func (e *eventDelegate) NotifyLeave(node *memberlist.Node) {
	uri := node.Addr.String()
	e.removePeer(uri)
	log.Print("Remove peer: " + uri)
	log.Printf("Current peers: %v", e.peers)
}

func (e *eventDelegate) NotifyUpdate(node *memberlist.Node) {
	log.Print("Update the node: %+v\n", node)
}

func (e *eventDelegate) removePeer(uri string) {
	for i := 0; i < len(e.peers); i++ {
		if e.peers[i] == uri {
			e.peers = append(e.peers[:i], e.peers[i+1:]...)
			i--
		}
	}
}

func main() {
	eventHandler := &eventDelegate{}
	conf := memberlist.DefaultLANConfig()
	conf.Events = eventHandler
	if addr := os.Getenv("MEMBERLIST_ADDR"); addr != "" {
		conf.AdvertiseAddr = addr
	}

	list, err := memberlist.Create(conf)
	if err != nil {
		panic("Failed to created memberlist: " + err.Error())
	}

	if nodes := os.Getenv("JOIN_TO"); nodes != "" {
		if _, err := list.Join(strings.Split(nodes, ",")); err != nil {
			panic("Failed to join cluster: " + err.Error())
		}
	}

	m := martini.Classic()
	m.Get("/:key", func(params martini.Params) string {
		return params["key"]
	})
	m.Run()
}
